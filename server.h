#ifndef SERVER_H
#define SERVER_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include "mongoose.h"

int startServer(int argc, char *argv[]);
int processServer(int argc, char *argv[]);
int checkRequest(struct mg_connection *conn);
void stopAll();
void stopServer();
void stopServerProcess();
void pollServer(int timeout);
extern int exit_flag;

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // SERVER_H
