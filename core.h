#ifndef CORE_H
#define CORE_H

#include <QtCore>
#include "myudp.h"
#include "server.h"

class Core : public QObject
{
    Q_OBJECT
public:
    explicit Core(QObject *parent = 0);
    void start(int argc, char *argv[]);
    static Core* instance();
    MyUDP myUDP;
    int checkRequest(struct mg_connection *conn);
	QSocketNotifier *m_pNot;

signals:
    void dataReady(MyHostAddress, QString);
    void quit();
    void stopAllSignal();
public slots:
    void processData(MyHostAddress, QString);
	void stopAllSlot();
private slots:
	void onData();

private:
};

#endif // CORE_H
