#include "requestcatcher.h"
#include <QtConcurrent>


RequestCatcher::RequestCatcher(QObject *parent) :
    QObject(parent)
{
    QtConcurrent::run(this, &RequestCatcher::catchRequests);
}

void RequestCatcher::catchRequests()
{
    qWarning() <<"started";
    while(qApp && this)
    {
        QTextStream s(stdin);
        QString value = s.readLine();
        if(!value.isEmpty())
            emit requestReady(value);
    }
}
