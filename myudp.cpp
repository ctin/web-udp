// myudp.cpp

#include "myudp.h"
#include <QDateTime>

MyUDP::MyUDP(QObject *parent) :
    QObject(parent)
{
    // create a QUDP socket
    socket = new QUdpSocket(this);
    // The most common way to use QUdpSocket class is
    // to bind to an address and port using bind()
    // bool QAbstractSocket::bind(const QHostAddress & address,
    //     quint16 port = 0, BindMode mode = DefaultForPlatform)
    socket->bind(5678);
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
}

ObjectMap MyUDP::modules() const
{
    return m_modules;
}

void MyUDP::setmodules(ObjectMap arg)
{
    if (m_modules != arg) {
        m_modules = arg;
        emit modulesChanged(arg);
    }
}

void MyUDP::setport(quint16 port)
{
    socket->bind(port);
}

void MyUDP::HelloUDP()
{
    QByteArray Data;
    Data.append("Hello from UDP");

    // Sends the datagram datagram
    // to the host address and at port.
    // qint64 QUdpSocket::writeDatagram(const QByteArray & datagram,
    //                      const QHostAddress & host, quint16 port)
    //qWarning() << "sending " << Data << "to port" << socket->localPort();
    socket->writeDatagram(Data, QHostAddress::LocalHostIPv6, socket->localPort());
}

void MyUDP::sendData(MyHostAddress server, QByteArray data)
{
    static int seqID;
    quint16 port = 8765;
    if(!data.contains("echo"))
    {
        data.append(QString("\n\"seqID\":%1").arg(seqID++));
        qWarning() << "sending data: " << data << "to server: "<< server.toString() << "port: " << port;
    }
    else
        qWarning() << "replying to client";
    
    socket->writeDatagram(data, server, port);
}

void MyUDP::readyRead()
{
    // when data comes in
    QByteArray buffer;
    buffer.resize(socket->pendingDatagramSize());

    MyHostAddress sender;
    quint16 senderPort;
    //qWarning() << "myUdp ready read";
    // qint64 QUdpSocket::readDatagram(char * data, qint64 maxSize,
    //                 QHostAddress * address = 0, quint16 * port = 0)
    // Receives a datagram no larger than maxSize bytes and stores it in data.
    // The sender's host address and port is stored in *address and *port
    // (unless the pointers are 0).

    socket->readDatagram(buffer.data(), buffer.size(),
                         &sender, &senderPort);

    qWarning() << "Message from: " << sender.toString() << " : " << senderPort;
    if(sender == QHostAddress::LocalHostIPv6)
    {
        if(buffer.contains("ping") || buffer.contains("hello"))
            socket->writeDatagram("hi!", sender, senderPort);
        return;
    }
    m_last = sender;
    QJsonObject &senderDataRef = m_modules[sender];

    QJsonParseError err;
    QJsonDocument jsonResponse = QJsonDocument::fromJson(buffer, &err);
    if(err.error != QJsonParseError::NoError)
    {
        qWarning() << "json err" << err.errorString() << "on message: " << buffer;
        return;
    }
    else
        qWarning() << "json ok" << jsonResponse.toJson();
    QJsonObject jsonObject = jsonResponse.object();
    //qWarning() << jsonObject;
    for(QJsonObject::const_iterator iter = jsonObject.constBegin();
        iter != jsonObject.constEnd();
        ++iter)
    {
        senderDataRef[iter.key()] = iter.value();
    }
	senderDataRef["time"] = QDateTime::currentDateTime().toString();

    if(buffer.contains("echo"))
    {
		QByteArray arr;
		arr.append(QString("echo: %1").arg(senderDataRef["echo"].toInt()));
		sendData(sender, arr);
	}
}
