#-------------------------------------------------
#
# Project created by QtCreator 2014-03-24T15:14:39
#
#-------------------------------------------------

QT       += core network
QT       -= gui
CFLAGS += -pthread
DEFINES += MONGOOSE_NO_CGI=1
TARGET = SQLServer
CONFIG   += console
CONFIG   -= app_bundle
TEMPLATE = app


SOURCES += main.cpp \
    myudp.cpp \
    mongoose.c \
    server.c \
    core.cpp

HEADERS += \
    myudp.h \
    mongoose.h \
    server.h \
    core.h

win32 {
    LIBS += -lws2_32
}
