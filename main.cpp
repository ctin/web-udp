#include <QCoreApplication>
#include <QTextCodec>
#include "core.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    qWarning() << "setcodec";
    QTextCodec *pCyrillicCodec = QTextCodec::codecForName("CP-1251");
    QTextCodec::setCodecForLocale(pCyrillicCodec);
    qWarning() << "starting core";
    Core c;
    c.start(argc, argv);


    return a.exec();
}
