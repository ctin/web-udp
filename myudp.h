// myudp.h

#ifndef MYUDP_H
#define MYUDP_H

#include <QObject>
#include <QUdpSocket>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

class MyHostAddress : public QHostAddress
{
public:
	MyHostAddress(const QString &arg)  : QHostAddress(arg) {}
	MyHostAddress() : QHostAddress() {}
    bool operator < (const MyHostAddress& arg) const { return this->toString() < arg.toString(); }
    //bool operator == (const MyHostAddress& arg) const { return this->toString() == arg.toString(); }
    //bool operator != (const MyHostAddress& arg) const { return this->toString() != arg.toString(); }
};

typedef QMap<MyHostAddress, QJsonObject> ObjectMap;

class MyUDP : public QObject
{
    Q_OBJECT
    Q_PROPERTY(ObjectMap modules READ modules WRITE setmodules NOTIFY modulesChanged)
public:
    explicit MyUDP(QObject *parent = 0);
    void setport(quint16 port);
    void HelloUDP();
    void sendData(MyHostAddress, QByteArray);
    MyHostAddress m_last;
public slots:
    void readyRead();

private:
    QUdpSocket *socket;

public:
    ObjectMap modules() const;

public slots:
    void setmodules(ObjectMap arg);

signals:
    void modulesChanged(ObjectMap arg);

private:
    ObjectMap m_modules;
};

#endif // MYUDP_H
