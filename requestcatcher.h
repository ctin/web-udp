#ifndef REQUESTCATCHER_H
#define REQUESTCATCHER_H

#include <QObject>

class RequestCatcher : public QObject
{
    Q_OBJECT
public:
    explicit RequestCatcher(QObject *parent = 0);
    void catchRequests();
signals:
    void requestReady(QString);

public slots:

};

#endif // REQUESTCATCHER_H
