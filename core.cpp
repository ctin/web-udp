#include "core.h"
#include "server.h"
#include <iostream>
#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <unistd.h> //Provides STDIN_FILENO

static const char* html_form =
        "<hmtl>"
        "<title>" "Server error" "</title>"
        "<body>" "Server error" "</body>"
        "</html>";
#define DEFAULT_CGI_PATTERN "**.cgi$|**.pl$|**.php$"

Core *instanse = NULL;

Core::Core(QObject *parent) :
    QObject(parent)
{
    qRegisterMetaType<MyHostAddress>("MyHostAddress");
    instanse = this;
    connect(this, SIGNAL(dataReady(MyHostAddress, QString)), this, SLOT(processData(MyHostAddress, QString)));
    connect(this, SIGNAL(quit()), qApp, SLOT(quit()));
    connect(this, SIGNAL(stopAllSignal()), this, SLOT(stopAllSlot()));
	
}

void Core::stopAllSlot()
{
    stopServerProcess();
    emit quit();
}
void Core::onData()
{
    QTextStream s(stdin);
	QString value = s.readLine();
	if(!value.isEmpty())
	{
		if(value.contains("quit") || value.contains("exit"))
		{
			printf("bye\n\n");
			emit stopAllSignal();
			return;
		}
		else if (value.contains("modules"))
		{
			QString text;
			text.append("{\n\"modules\":[\n");
			for(ObjectMap::const_iterator iter = myUDP.modules().constBegin();  iter != myUDP.modules().constEnd();  iter++) 
			{
				QJsonDocument doc(iter.value());
				text.append("{\"address\": \"" + iter.key().toString() + "\n\"," 
				+ "\"value\":" + doc.toJson(QJsonDocument::Indented) + "},\n");
			}
			if(text.endsWith("},\n")) 
			{
				text.chop(2);
				text.append("\n");
			}
			text.append("]\n}");
			qWarning() << text;
		}
		else
			emit dataReady(myUDP.m_last, value);
	}
}

void Core::processData(MyHostAddress addr, QString arg)
{
    QByteArray data;
    data.append(arg);
    myUDP.sendData(addr, data);
}


void Core::start(int argc, char *argv[])
{
    Q_ASSERT_X(instanse, "creating core", "core is singleton");
    myUDP.HelloUDP();

	m_pNot = new QSocketNotifier(STDIN_FILENO, QSocketNotifier::Read, this);
	connect(m_pNot, SIGNAL(activated(int)), this, SLOT(onData()));
	m_pNot->setEnabled(true);
	QtConcurrent::run(&processServer, argc, argv);

}

Core* Core::instance()
{
    return instanse;
}

void stopAll()
{
	qApp->quit();
}

int checkRequest(struct mg_connection *conn)
{
    int match = mg_match_prefix(DEFAULT_CGI_PATTERN, strlen(DEFAULT_CGI_PATTERN), conn->uri);
    if(match < 0)
        return 0;
    MyUDP *pMyUDP = &Core::instance()->myUDP;

    QString url(conn->uri);
    if(url.contains("getModuleList"))
    {
        QString text;
        text.append("{\n\"modules\":[\n");
        for(ObjectMap::const_iterator iter = pMyUDP->modules().constBegin();  iter != pMyUDP->modules().constEnd();  iter++) 
		{
			QDateTime dateTime(QDateTime::fromString(iter.value()["time"].toString()));
			int lastResponseTime = dateTime.secsTo(QDateTime::currentDateTime());
			int idleTime = iter.value()["idleTime"].toInt();
			if(idleTime > 0)
			{
				if(lastResponseTime > idleTime + 50)
					continue;
			}
			QJsonDocument doc(iter.value());
			text.append("{\"address\": \"" + iter.key().toString() + "\"," 
				+ "\"lastResponseTime\":" + QString::number(lastResponseTime) + ","
				+ "\"value\":" + doc.toJson(QJsonDocument::Compact) + "},\n");
		}
	if(text.endsWith("},\n")) 
	{
		text.chop(2);
		text.append("\n");
        }
	text.append("]\n}");
        mg_send_data(conn, text.toUtf8().data(), text.toUtf8().length());
    }
    else if(url.contains("sendStringToClient"))
    {
	QString data(conn->content);
	QStringList lines = data.split('\n');
	if(lines.isEmpty())
	{
		qWarning() << "data err:" << data;
		mg_send_data(conn, html_form, strlen(html_form));
		return 0;
	}
	QString last = lines.at(0);
	QString addr;
        int firstInd = last.indexOf("$$");
        addr = last.left(firstInd);
        addr = addr.remove(0, addr.indexOf("addr:") + strlen("addr:"));
        QString val = last.remove(0, firstInd + 2);
        val = last.left(last.indexOf("$$"));
	Core::instance()->processData(MyHostAddress(addr), val);
	mg_send_data(conn, "ok", strlen("ok")); 
    }
    else
        mg_send_data(conn, html_form, strlen(html_form));
    return 1;
}
